# Example of how to to auth in streamlit

- `python3 -m venv venv`
- `. venv/bin/activate`
- `pip install -r requirements.txt`
- `streamlit run auth_app.py`