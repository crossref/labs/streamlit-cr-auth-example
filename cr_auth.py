import time
import logging
import requests
#from requests.exceptions import ConnectionError

import streamlit as st
from PIL import Image

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()



CR_AUTH_ENDPOINT = "https://doi.crossref.org/servlet/login"

CR_LOGO_NOTEXT = image = Image.open("./cr_logo_notext.png")

# if 'tarpit' not in st.session_state:
#     logger.info("Initialising tarpit")
#     st.session_state.tarpit = 0


def tarpit():
    # keep increasing delay before showing login screen in case
    # a bot is trying to brute-force passwords
    st.session_state.tarpit += 2
    logger.warning(f"increasing tarpit to: {st.session_state.tarpit}")
    time.sleep(st.session_state.tarpit)

def first_login():
    return "auth" not in st.session_state


def authenticated():
    return st.session_state.auth


def role_selected():
    return st.session_state.role_selected


def all_credenitials_provided():
    return authenticated() and role_selected()


def init_login():
    st.session_state.auth = False
    st.session_state.role_selected = False
    if 'tarpit' not in st.session_state:
        logger.info("Initialising tarpit")
        st.session_state.tarpit = 0



def cr_authenticate(user_name, password):
    try:
        res = requests.post(
            url=CR_AUTH_ENDPOINT, data={"usr": user_name, "pwd": password}
        ).json()
        if res["authenticated"]:
            st.session_state.user_name = user_name
            st.session_state.available_roles = res["roles"]
            st.session_state.auth = True
            return True
        return False
    except requests.exceptions.ConnectionError as e:
        st.error("Network error. Is your netowrk connection active?")
        return False

        


def fake_authenticate(user_name, password):
    time.sleep(2)
    st.session_state.user_name = user_name
    st.session_state.available_roles = ["root", "star-lord", "creftest"]
    st.session_state.auth = True
    return True


def authenticate(user_name, password):
    with st.spinner(text="Authenticating..."):
        authenticated = (
            fake_authenticate(user_name=user_name, password=password)
            if st.session_state.debug
            else cr_authenticate(user_name=user_name, password=password)
        )
        if not authenticated:
            st.error(f"{user_name} failed to authenticate")
            tarpit()
            init_login()
           

def select_role():
    st.session_state.role_selected = True


def show_role_selector():
    st.image(CR_LOGO_NOTEXT)
    st.success(f"Authenticated as {st.session_state.user_name}")
    initial_options = [None]
    initial_options += st.session_state.available_roles
    role = st.selectbox(label="Select an assigned role", options=initial_options, key="role")
    st.button(label="Select", on_click=select_role, disabled=not role)


def show_login():
    st.image(CR_LOGO_NOTEXT)
    st.markdown("""---""")
    st.subheader("Crossref Login")
    user_name = st.text_input(label="Username", placeholder="name@example.com")
    password = st.text_input(label="Password", type="password")
    credentials_entered = user_name and password
    st.button(
        label="Login",
        on_click=authenticate,
        kwargs={"password": password, "user_name": user_name},
        disabled=not credentials_entered,
    )

    st.markdown(
        "[FORGOTTON YOUR PASSWORD?](https://authenticator.crossref.org/reset-password/)"
    )


def show_logged_in_as():
    st.text_input(label="User", placeholder=st.session_state.user_name, disabled=True)
    st.selectbox(
        label="Role",
        options=st.session_state.available_roles,
        on_change=select_role,
        key="role",
        index=st.session_state.available_roles.index(st.session_state.role),
    )
    st.button(label="Logout", on_click=init_login)
    st.markdown("""---""")
